package dv.spring.kotlin.entity

import javax.persistence.*
import javax.validation.constraints.Email

@Entity
data class Customer (
        override var name: String,
        override var email: String,
        override var userStatus: UserStatus = UserStatus.PENDING
) : User {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var shippingAddress = mutableListOf<Address>()
    @OneToOne
    lateinit var billingAddress: Address
    @OneToOne
    lateinit var defaultAddress: Address
}
