package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectedProducts = mutableListOf<SelectedProduct>()
    @OneToOne
    lateinit var customer: Customer
    @OneToOne
    lateinit var shippingAddress: Address
}

