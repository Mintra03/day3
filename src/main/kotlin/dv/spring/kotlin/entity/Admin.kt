package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Admin (
        override var name: String,
        override var email: String,
        override var userStatus: UserStatus = UserStatus.PENDING
): User {
    @Id
    @GeneratedValue
    var id:Long? = null
}