package dv.spring.kotlin.entity

import com.sun.imageio.plugins.common.ImageUtil
import java.beans.BeanDescriptor
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Product (var name: String,
                    var description: String,
                    var price: Double,
                    var amountInStock: Int,
                    var imageUrl: String?=null) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    lateinit var manufacturer: Manufacturer
    constructor( name: String,
                 description: String,
                 price: Double,
                 amountInStock: Int,
                 manufacturer: Manufacturer,
                 imageUrl: String?):
            this(name,description,price,amountInStock,imageUrl) {
        this.manufacturer = manufacturer
    }
}