package dv.spring.kotlin.entity

interface User {
    var name: String
    var email: String
    var userStatus: UserStatus
}