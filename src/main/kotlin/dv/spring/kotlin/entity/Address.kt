package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class Address (var homeAddress: String,
                    var subdistrict: String,
                    var district: String,
                    var province: String,
                    var postCode: String){
    @Id
    @GeneratedValue
    var id:Long? = null
}