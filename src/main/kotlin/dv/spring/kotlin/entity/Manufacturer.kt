package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class Manufacturer (var name: String, var telNo: String) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "manufacturer")
    var products = mutableListOf<Product>()
//    lateinit var address : Address
}