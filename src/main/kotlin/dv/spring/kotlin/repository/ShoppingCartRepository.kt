package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long>