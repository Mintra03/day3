package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Product
import org.springframework.data.repository.CrudRepository

interface ProductRepository: CrudRepository<Product,Long>