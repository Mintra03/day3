package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>