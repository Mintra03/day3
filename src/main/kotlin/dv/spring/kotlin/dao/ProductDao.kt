package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Product


interface ProductDao {
    fun getProduct(): List<Product>
}