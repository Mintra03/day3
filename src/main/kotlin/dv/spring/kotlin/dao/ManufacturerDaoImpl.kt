package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaoImpl:ManufacturerDao{
    override fun getManufacturers(): List<Manufacturer> {
        return mutableListOf(
                Manufacturer("iPhone","053123456"),
                Manufacturer("Samsung","555666777888"),
                Manufacturer("CAMT","0000000"))
    }
}